/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overloadingexeremcasa;

/**
 *
 * @author Dimitri
 */
public class Calculator {
    protected double fNum1;
    protected double fNum2;
    
    public Calculator(){
        
    }
    public Calculator(double fVal, double fVal2){
        this.fNum1 = fVal;
        this.fNum2 = fVal2;
    }

    public double getfNum1() {
        return fNum1;
    }

    public void setfNum1(double fNum1) {
        this.fNum1 = fNum1;
    }

    public double getfNum2() {
        return fNum2;
    }

    public void setfNum2(double fNum2) {
        this.fNum2 = fNum2;
    }
    
    public double soma(){
        return this.fNum1+this.fNum2;
    }
    public static double soma(double fVal, double fVal2){
        return fVal+fVal2;
    }
    
    public double subtracao(){
        return this.fNum1 - this.fNum2;
    }
    public static double subtracao(double fVal, double fVal2){
        return fVal - fVal2;
    }
    
    public double multiplicacao(){
        return this.fNum1 * this.fNum2;
    }
    public static double multipicacao(double fVal,double fVal2){
        return fVal * fVal2;
    }
    
    public double divisao(){
        return this.fNum1 / this.fNum2;
    }
    public static double divisao(double fVal, double fVal2){
        return fVal / fVal2;
    }
    
    public String getAllResults(){
        return "sum: "+this.soma()+"\nsub: "+this.subtracao()+"\nmult: "+this.multiplicacao()+"\ndiv: "+this.divisao();
    }
}
