/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overloadingexeremcasa;

import java.util.Scanner;

/**
 *
 * @author Dimitri
 */
public class OverloadingExerEmCasa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        Object object = new Object();
        System.out.println();
        
        //TIPOS DE CRIAÇÃO DE OBJETOS
        /*Calculator obj = new AdvancedCalc();
        Calculator obj2 = obj;
        
        Calculator obj = new Calculator();
        AdvancedCalc obj2 = (AdvancedCalc)obj;

        AdvancedCalc obj = new Calculator();//E R R O R*/
        
        //aqui obj2 recebe a REFERÊNCIA de obj
        Calculator obj = new AdvancedCalc();
        AdvancedCalc obj2 = (AdvancedCalc) obj;//fazendo casting
        
        //criando objetos de calculadora básica e avançada
        /*Calculator calc = new Calculator(2,4);
        Calculator calc2 = new AdvancedCalc(2,4);
        
        if (calc instanceof Calculator) {
            System.out.println(calc.soma());
        } 
        if (calc2 instanceof AdvancedCalc) {
            //fazendo o casting do objeto para chamar o método desejado
            double fResExp = ((AdvancedCalc) calc2).exponentiation();
            System.out.println(fResExp);
        }*/
        
        //ATIVIDADE 2 DO CLASSROM
        Scanner sc = new Scanner(System.in);
        System.out.println("write 'Calculator' or 'Advanced' to use one of the two calculators: ");
        String option = sc.next().toLowerCase();
        
        Calculator objCalc;
        
        switch (option) {
            case "advanced":
                objCalc = new AdvancedCalc(4, 2);
                break;
            case "calculator":
                objCalc = new Calculator(4, 2);
                break;
            default:
                System.out.println("error down here");
                throw new AssertionError();
        }
        
        if (objCalc instanceof AdvancedCalc) {
            System.out.println(((AdvancedCalc) objCalc).getAllAdvancedCalcResults());
        } else if(objCalc instanceof Calculator){
            System.out.println(objCalc.getAllResults());
        }else{
            System.out.println("error");
        }
        
        
        
    }
    
}
