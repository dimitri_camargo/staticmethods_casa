/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overloadingexeremcasa;

/**
 *
 * @author Dimitri
 */
public class AdvancedCalc extends Calculator{
    public AdvancedCalc(){
        //Calling Empty Constructor from superclass
        super();
    }
    
    public AdvancedCalc(double fNum1, double fNum2){
        super.fNum1 = fNum1;
        super.fNum2 = fNum2;
    }
    
    public double sqrt(){
        return Math.sqrt(super.fNum1);
    }
    
     //exponentiation methods
    public double exponentiation(){
        return Math.pow(super.fNum1, super.fNum2);
    }
    
    public double exponentiation(double fNum1, double fNum2){
        return Math.pow(fNum2, fNum2);
    }
    
    public double sine(double fNum){
        return Math.sin(fNum);
    }
    
    public double cosine(double fNum){
        return Math.cos(fNum);
    }
    
    public double tangent(double fNum){
        return Math.tan(fNum);
    }

    public String getAllAdvancedCalcResults(){
        return super.getAllResults()+"\nsqrt: "+this.sqrt()+"\nexp: "+this.exponentiation();
    }
}
